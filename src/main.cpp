/**
 * Copyright 2022-2024 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <boost/program_options.hpp>
#include <iostream>
#include <regex>

#include "JournalReader.h"
#include "version.h"

namespace bpo = boost::program_options;

void exclusiveOptions(const bpo::variables_map &vm, const char* a, const char* b) {
	if (vm.count(a) && !vm[a].defaulted() && vm.count(b) && !vm[b].defaulted()) {
		throw JournalReaderException(ErrCodes::INVALID_ARGUMENT_ERROR, "Options '" + std::string(a) + "' and '" + std::string(b) + "' are mutually exclusive.");
	}
}

void validateCursor(const std::string &cursor) {
	if (!std::regex_match(cursor, std::regex(JournalReader::CURSOR_REGEX, std::regex_constants::icase))) {
		throw JournalReaderException(ErrCodes::INVALID_ARGUMENT_ERROR, "Invalid cursor format: " + cursor);
	}
}

int main(int argc, char **argv) {
	bpo::options_description general("General options");
	general.add_options()
		("help,h", "display help message")
		("version,v", "display app version");
	bpo::options_description journal("Journal options");
	journal.add_options()
		("number,n", bpo::value<uint64_t>(), "print last number of journal records (mutually exclusive with -s)")
		("start-cursor,s", bpo::value<std::string>(), "print records from specified cursor (mutually exclusive with -n)")
		("end-cursor,e", bpo::value<std::string>(), "print records to specified cursor");
	bpo::options_description output("Output options");
	output.add_options()
		("json,j", bpo::bool_switch()->default_value(false), "output as json");
	bpo::options_description desc("Available options");
	desc.add(general).add(journal).add(output);
	bpo::variables_map vm;
	try {
		bpo::store(bpo::parse_command_line(argc, argv, desc), vm);
		bpo::notify(vm);

		exclusiveOptions(vm, "number", "start-cursor");

		if (vm.empty() || vm.count("help")) {
			std::cout << "usage: " << PROJECT_NAME << " [options]\n" << std::endl;
			std::cout << desc << std::endl;
		} else if (vm.count("version")) {
			std::cout << "v" << PROJECT_VERSION << std::endl;
		} else {
			JournalReader::Params params = JournalReader::Params();
			if (vm.count("number")) {
				params.count = vm["number"].as<uint64_t>();
			}
			if (vm.count("start-cursor")) {
				std::string startCursor = vm["start-cursor"].as<std::string>();
				validateCursor(startCursor);
				params.startCursor = startCursor;
			}
			if (vm.count("end-cursor")) {
				std::string endCursor = vm["end-cursor"].as<std::string>();
				validateCursor(endCursor);
				params.endCursor = endCursor;
			}
			params.json = vm["json"].as<bool>();
			JournalReader reader(params);
			reader.readRecords();
			return EXIT_SUCCESS;
		}
	} catch (const JournalReaderException &e) {
		std::cerr << e.what() << std::endl;
		return e.getCode();
	} catch (const std::exception &e) {
		std::cerr << e.what() << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
