/**
 * Copyright 2022-2024 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once

#include <cstdint>
#include <string>

/**
 * Journal record class
 */
class JournalRecord {
public:
	/**
	 * Constructor
	 */
	JournalRecord() {};

	/**
	 * Returns record timestamp
	 * @return std::string Record timestamp
	 */
	std::string getTimestamp() const {
		return this->timestamp;
	}

	/**
	 * Sets record timestamp
	 * @param timestamp Record timestamp
	 */
	void setTimestamp(const std::string &timestamp) {
		this->timestamp = timestamp;
	}

	/**
	 * Returns record hostname
	 * @return std::string Record hostname
	 */
	std::string getHostname() const {
		return this->hostname;
	}

	/**
	 * Sets record hostname
	 * @param hostname Record hostname
	 */
	void setHostname(const std::string &hostname) {
		this->hostname = hostname;
	}

	/**
	 * Returns record process ID
	 * @return int64_t Record process ID
	 */
	int64_t getPid() const {
		return this->pid;
	}

	/**
	 * Sets record process ID
	 * @param pid Record process ID
	 */
	void setPid(const std::string &pid) {
		try {
			this->pid = static_cast<int64_t>(std::stoll(pid));
		} catch (const std::invalid_argument &e) {
			this->pid = -1;
		}
	}

	/**
	 * Returns record identifier
	 * @return std::string Record identifier
	 */
	std::string getIdentifier() const {
		return this->identifier;
	}

	/**
	 * Sets record identifier
	 * @param identifier Record identifier
	 */
	void setIdentifier(const std::string &identifier) {
		this->identifier = identifier.length() == 0 ? "unknown" : identifier;
	}

	/**
	 * Returns record priority/severity
	 * @return int8_t Record priority
	 */
	int8_t getPriority() const {
		return this->priority;
	}

	/**
	 * Sets record priotity/severity
	 * @param priority Record priority
	 */
	void setPriority(const std::string &priority) {
		try {
			this->priority = static_cast<int8_t>(std::stoi(priority));
		} catch (const std::invalid_argument &e) {
			this->priority = -1;
		}
	}

	/**
	 * Returns record message
	 * @return std::string Record message
	 */
	std::string getMessage() const {
		return this->message;
	}

	/**
	 * Sets record message
	 * @param message Record message
	 */
	void setMessage(const std::string &message) {
		this->message = message;
	}
private:
	/// Record timestamp
	std::string timestamp;
	/// Hostname
	std::string hostname;
	/// PID
	int64_t pid;
	/// Identifier
	std::string identifier;
	/// Syslog-compatible priority
	int8_t priority;
	/// Record message
	std::string message;
};
