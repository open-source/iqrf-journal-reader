/**
 * Copyright 2022-2024 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once

#include <cstdint>
#include <cstring>
#include <iostream>
#include <list>
#include <string>
#include <stdexcept>

#include <nlohmann/json.hpp>
#include <systemd/sd-journal.h>
#include <time.h>

#include "ErrCodes.h"
#include "JournalReaderException.h"
#include "JournalRecord.h"

using json = nlohmann::json;

/**
 * Journal reader class
 */
class JournalReader {
public:
	/**
	 * Input parameters struct
	 */
	struct Params {
		/// Number of records
		uint64_t count;
		/// Cursor to start printing from
		std::string startCursor;
		/// Cursor to print to
		std::string endCursor;
		/// Print as json
		bool json;
	};

	/// Journal entry cursor regex
	static constexpr const char* CURSOR_REGEX = "^s=([0-9a-f]{32});i=([0-9a-f]+);b=([0-9a-f]{32});m=([0-9a-f]+);t=([0-9a-f]+);x=([0-9a-f]{16})$";

	/**
	 * Delete base constructor
	 */
	JournalReader() = delete;

	/**
	 * Full constructor
	 * @param params Input parameters
	 */
	JournalReader(Params &params) : params(params) {};

	/**
	 * Destructor
	 */
	~JournalReader() {}

	/**
	 * Attempts to read and print requested records
	 */
	void readRecords();
private:
	/**
	 * Processe and prints current journal record
	 * @param journal Journal
	 */
	void processRecord(sd_journal *journal);

	/**
	 * Retrieves journal record and parses to JournalRecord object
	 * @param journal Journal
	 * @return JournalRecord Parsed journal record
	 */
	JournalRecord getJournalRecord(sd_journal *journal);

	/**
	 * Returns current journal record cursor as string
	 * @param journal Journal
	 * @return std::string Journal record cursor
	 */
	std::string getCursor(sd_journal *journal);

	/**
	 * Returns journal record field value as string
	 * @param journal Journal
	 * @param field Field name
	 * @return std::string Journal record field value
	 */
	std::string getFieldValue(sd_journal *journal, const std::string &field);

	/**
	 * Returns journal record timestamp as string
	 * @param journal Journal
	 * @return std::string Journal record timestamp
	 */
	std::string getTimestamp(sd_journal *journal);

	/**
	 * Prints journal record in journald format
	 * @param record Journal record
	 */
	void printRecord(JournalRecord &record);

	/**
	 * Prints all records as json object
	 */
	void printJson();

	/**
	 * Handles errors and cleans up
	 * @param journal Journal
	 * @param code Error code
	 * @param errnum Journal API errno
	 * @param error Error message
	 */
	void handleError(sd_journal *journal, const ErrCodes &code, const int8_t &errnum, const std::string &error);

	/// Journal reader input parameters
	Params &params;
	/// Journal start cursor
	const char *startCursor = nullptr;
	/// Journal end cursor
	const char *endCursor = nullptr;
	/// Cursor of the first printed record
	std::string firstCursor;
	/// Cursor of the last printed record
	std::string lastCursor;
	/// Journal records
	std::list<JournalRecord> records;
};
