/**
 * Copyright 2022-2024 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "JournalReader.h"

void JournalReader::handleError(sd_journal *journal, const ErrCodes &code, const int8_t &errnum, const std::string &error) {
	char *e = std::strerror(-errnum);
	sd_journal_close(journal);
	throw JournalReaderException(code, error + ": " + (e ? std::string(e) : "unknown error"));
}

void JournalReader::readRecords() {
	sd_journal *journal;
	int ret;
	tzset();

	ret = sd_journal_open(&journal, SD_JOURNAL_LOCAL_ONLY);
	if (ret < 0) {
		handleError(journal, ErrCodes::JOURNAL_ERROR, ret, "Failed to open journal");
	}

	// get cursors
	this->startCursor = this->params.startCursor.empty() ? nullptr : this->params.startCursor.c_str();
	this->endCursor = this->params.endCursor.empty() ? nullptr : this->params.endCursor.c_str();

	if (this->params.count) {
		if (this->endCursor) {
			ret = sd_journal_seek_cursor(journal, this->endCursor);
			if (ret < 0) {
				handleError(journal, ErrCodes::JOURNAL_ERROR, ret, "Failed to seek to requested end cursor");
			}
			ret = sd_journal_previous(journal);
			if (ret < 0) {
				handleError(journal, ErrCodes::JOURNAL_ERROR, ret, "Failed to seek to requested end cursor");
			}
		} else {
			ret = sd_journal_seek_tail(journal);
			if (ret < 0) {
				handleError(journal, ErrCodes::JOURNAL_ERROR, ret, "Failed to seek to end of journal");
			}
		}
		ret = sd_journal_previous_skip(journal, this->params.count + 1);
		if (ret < 0) {
			handleError(journal, ErrCodes::JOURNAL_ERROR, ret, "Failed to skip the beginning of requested records");
		}
	} else {
		if (this->startCursor) {
			ret = sd_journal_seek_cursor(journal, this->startCursor);
			if (ret < 0) {
				handleError(journal, ErrCodes::JOURNAL_ERROR, ret, "Failed to seek to requested start cursor");
			}
			ret = sd_journal_next(journal);
			if (ret < 0) {
				handleError(journal, ErrCodes::JOURNAL_ERROR, ret, "Failed to seek to requested start cursor");
			}
		} else {
			ret = sd_journal_seek_head(journal);
			if (ret < 0) {
				handleError(journal, ErrCodes::JOURNAL_ERROR, ret, "Failed to seek to beginning of journal");
			}
			ret = sd_journal_next(journal);
			if (ret < 0) {
				handleError(journal, ErrCodes::JOURNAL_ERROR, ret, "Failed to seek to requested start cursor");
			}
		}
	}

	unsigned int cnt = 0;

	while (sd_journal_next(journal) > 0) {
		if (this->firstCursor.empty()) {
			this->firstCursor = getCursor(journal);
		}
		if (this->endCursor && sd_journal_test_cursor(journal, this->endCursor)) {
			if (this->startCursor) {
				processRecord(journal);
				cnt++;
			}
			break;
		}
		processRecord(journal);
		cnt++;
	}

	sd_journal_close(journal);

	if (this->params.json) {
		printJson();
	} else {
		if (cnt == 0) {
			std::cout << "No new available records." << std::endl;
		} else {
			std::cout
				<< "first cursor: " << this->firstCursor << std::endl
				<< "last cursor: " << this->lastCursor << std::endl;
		}
	}
}

void JournalReader::processRecord(sd_journal *journal) {
	this->lastCursor = getCursor(journal);
	JournalRecord record = getJournalRecord(journal);
	if (this->params.json) {
		this->records.push_back(record);
	} else {
		printRecord(record);
	}
}

JournalRecord JournalReader::getJournalRecord(sd_journal *journal) {
	JournalRecord record;
	record.setTimestamp(getTimestamp(journal));
	record.setHostname(getFieldValue(journal, "_HOSTNAME"));
	record.setPid(getFieldValue(journal, "_PID"));
	std::string identifier = getFieldValue(journal, "SYSLOG_IDENTIFIER");
	if (identifier.length() == 0) {
		identifier = getFieldValue(journal, "_COMM");
	}
	record.setIdentifier(identifier);
	record.setPriority(getFieldValue(journal, "PRIORITY"));
	record.setMessage(getFieldValue(journal, "MESSAGE"));
	return record;
}

std::string JournalReader::getCursor(sd_journal *journal) {
	char *aux = nullptr;
	int ret = sd_journal_get_cursor(journal, &aux);
	if (ret < 0) {
		handleError(journal, ErrCodes::JOURNAL_ERROR, ret, "Failed to get record cursor");
	}
	std::string cursor(aux);
	delete aux;
	return cursor;
}

std::string JournalReader::getFieldValue(sd_journal *journal, const std::string &field) {
	const void *aux = nullptr;
	size_t len;
	int ret = sd_journal_get_data(journal, field.c_str(), &aux, &len);
	if (ret < 0) {
		return std::string();
	}
	std::string value(static_cast<const char *>(aux), len);
	size_t pos = value.find('=');
	if (pos != std::string::npos) {
		value = value.substr(pos + 1);
	}
	return value;
}

std::string JournalReader::getTimestamp(sd_journal *journal) {
	uint64_t timestamp;
	char string[16];
	int ret = sd_journal_get_realtime_usec(journal, &timestamp);
	if (ret < 0) {
		return std::string("unknown-time");
	}
	timestamp /= 1000000;
	struct tm time;
	localtime_r((time_t *)&timestamp, &time);
	strftime(string, 16, "%b %d %T", &time);
	return std::string(string);
}

void JournalReader::printRecord(JournalRecord &record) {
	std::cout
		<< record.getTimestamp()
		<< " " << record.getHostname()
		<< " " << record.getIdentifier()
		<< "[" << record.getPid() << "]"
		<< ": " << record.getMessage()
		<< std::endl;
}

void JournalReader::printJson() {
	json document = {
		{"firstCursor", this->firstCursor},
		{"lastCursor", this->lastCursor},
		{"records", json::array()}
	};
	for (; !this->records.empty(); this->records.pop_front()) {
		JournalRecord record = this->records.front();
		document["records"].push_back({
			{"timestamp", record.getTimestamp()},
			{"hostname", record.getHostname()},
			{"identifier", record.getIdentifier()},
			{"pid", record.getPid()},
			{"priority", record.getPriority()},
			{"message", record.getMessage()}
		});
	}
	std::cout << document.dump(4) << std::endl;
}
