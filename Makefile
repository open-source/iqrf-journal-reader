build: clean
	cmake -Bbuild -H. -DCMAKE_BUILD_TYPE=Debug
	cmake --build build

deb-package:
	debuild -b -uc -us -tc

clean:
	rm -rf build && mkdir build
